#!/usr/bin/env bash

# set cachedir and msailor path
cachedir="$HOME/.cache/msailor"
mkdir -p "$cachedir"
rm "$cachedir"/tmp* 2> /dev/null

if [[ $XDG_CONFIG_HOME == '' ]]; then
    msailordir="$HOME/.config/msailor"
else
    msailordir="$XDG_CONFIG_HOME/msailor"
fi
mkdir -p "$msailordir"

# create config's var
novideo=''
editor=$EDITOR
function config {
    exist=false
    config=''
    [[ -f "$msailordir"/config ]] && exist=true && config="$msailordir"/config

    if [[ $exist == true ]]; then
        # todo
        grep -q "set novideo" "$msailordir"/config && novideo='--no-video'
        grep -q "set editor=" "$msailordir"/config && tmp=$(grep 'set editor=' "$msailordir"/config) && editor="${tmp/set editor=/}"
    fi
}

# print -h/--help
function help {
    echo "
        config folder: $HOME/.msailor
        quickmark file: $HOME/.msailor/quickmark
            file format:
                name of the media [url]
                name of the media [url]
                name of the media [url]
                ...
        source file: $HOME/.mediasailor/source
            file format:
                [url]
                [url]
                [url]
                ...
        lists: $HOME/.mediasailor/list/*
            same file format as quickmark


        console:
            ./mediasailor -h/--help
            ./mediasailor [url]
            ./mediasailor [magnet]
            ./mediasailor ':yt [arg]'
            ./mediasailor ':ytListImport [arg]'
            ./mediasailor ':sc [arg]'
            ./mediasailor ':magnet [arg]'
            ./mediasailor ':indexwp [arg]'
            ...

        menu:
            :help                   (show help)
            :update                 (update app, you need to restart app after that)
            :sync                   (sync repositories)
            :push                   (push msailor folder to your repository doing a 'commit --amend' and 'push -f')
            :config-edit         (open config in the editor configured in config (\$EDITOR by default))
            :quickmark-add [arg]    (add latest media played to quickmark)
            :quickmark-del [arg]
            :quickmark-edit [arg]   (open quickmark in the editor configured in config (\$EDITOR by default))
            :list-add [arg]         (add latest media played to a list)
                                            (it will show your lists so you can choose where to add the media, or you can write a new list to be added)
                                            (WARNING: magnets do not work properly inside lists)

            :list-del [arg]         (delete entire list)
            :list-editor [arg]      (open list in the editor configured in config (\$EDITOR by default))

            :yt [arg]
            :ytListImport [arg]     (add yt list to a local list in ~/.msailor/list/  |  only add first 100 videos of a list)
                                            (use the link address https://www.youtube.com/playlist?list=...)

            :sc [arg]               (soundcloud)

            :torrent [arg]
            :indexwp [arg]          (scrap index web pages from your \"source\" file if you want all, do not pass arguments)
    " | less;
}

# sync all the repos in .msailordir
function sync {
    exist=false
    config=''
    rm "$msailordir"/sync/* -rf
    mkdir -p "$msailordir"/sync
    [[ -f "$msailordir"/config ]] && exist=true && config="$msailordir"/config
    [[ $exist == false ]] && echo "No config" && notify-send "No config" && exit 0

    [[ $exist == true ]] && (cat "$config" | grep 'sync=' | awk -F'sync=' '{print $NF}') | awk -F'/' '{print $0" $HOME/.config/msailor/sync/"$4}' | eval git clone $(cat)
}

# push changes to user's repo
function push {
    git -C "$msailordir" stage . && git -C "$msailordir" commit -m 'msailor auto push' && git -C "$msailordir" push
}

# scrap youtube and play media
function yt {
    baseurl="https://www.youtube.com"
    query="${query// /+}"
    curl -s "$baseurl/results?search_query=$query" > "$cachedir"/tmpHtml

    # get title (only videos, not lists)
    grep -oP '(?<="title":{"runs":\[{"text":").+?(?=")' "$cachedir"/tmpHtml > "$cachedir"/tmpTitle

    # get url
    grep -oP '(?<="commandMetadata":{"webCommandMetadata":{"url":").+?(?=")'  "$cachedir"/tmpHtml | grep 'watch?v=' > "$cachedir"/tmpUrl
    ## remove lists
    grep -v 'list=' "$cachedir"/tmpUrl > "$cachedir"/tmp && cat "$cachedir"/tmp > "$cachedir"/tmpUrl

    # select option
    cat "$cachedir"/tmpTitle > "$cachedir"/tmpMenu
    menu

    option=$(cat "$cachedir"/tmpTitle | grep -n -m 1 "$query" | cut -d: -f1)
    query="$baseurl$(cat "$cachedir"/tmpUrl | sed "${option}q;d")"

    methods
}

# import yt list to mediasailor format
function ytListImport {
    curl -s "$query" > "$cachedir"/tmpHtml

    # get list title
    title=$(grep -oP '(?<="metadata":{"playlistMetadataRenderer":{"title":").+?(?=")' "$cachedir"/tmpHtml)

    mkdir -p "$msailordir"/list
    rm "$msailordir"/list/"$title" && touch "$msailordir"/list/"$title"

    # get url
    grep -oP '(?<="commandMetadata":{"webCommandMetadata":{"url":").+?(?=")' "$cachedir"/tmpHtml | grep 'watch?v=' > "$cachedir"/tmpUrl
    grep 'index=' "$cachedir"/tmpUrl > "$cachedir"/tmp && cat "$cachedir"/tmp > "$cachedir"/tmpUrl

    # get video title
    grep -oP '(?<="title":{"runs":[{"text":").+?(?=")' "$cachedir"/tmpHtml > "$cachedir"/tmpTitle

    ii=0
    while read -r line
    do
        ii=$((ii+1))
        echo "$(sed "${ii}q;d" "$cachedir"/tmpTitle) https://youtube.com$(sed "${ii}q;d" "$cachedir"/tmpUrl)" >> "$msailordir"/list/"$title"
    done < "$cachedir"/tmpUrl
}

# scrap soundcloud and play media
function sc {
    baseurl="https://soundcloud.com"
    query="${query// /+}"
    curl -s "$baseurl/search?q=$query" > "$cachedir"/tmpHtml

    # get title (only videos, not channels)
    grep -oP '<li><h2><a href="/.+/.+">.+</a></h2></li>' "$cachedir"/tmpHtml | grep -oP '(?<=">).+?(?=<)' > "$cachedir"/tmpTitle

    # get url
    grep -oP '(?<=<li><h2><a href=")/.+?/.+?(?=">)'  "$cachedir"/tmpHtml > "$cachedir"/tmpUrl

    # select option
    cat "$cachedir"/tmpTitle > "$cachedir"/tmpMenu
    menu

    option=$(cat "$cachedir"/tmpTitle | grep -n -m 1 "$query" | cut -d: -f1)
    query="$baseurl$(cat "$cachedir"/tmpUrl | sed "${option}q;d")"

    methods
}

# scarp torrent web pages for torrents
function torrent { # this code is taken from notflix. All credits for them.
    baseurl="https://1337x.wtf"

    query="$(echo $query | sed 's/ /+/g')"

    curl -s $baseurl/search/$query/1/ > "$cachedir"/tmpHtml

    # Get Titles
    grep -o '<a href="/torrent/.*</a>' "$cachedir"/tmpHtml |
      sed 's/<[^>]*>//g' > "$cachedir"/tmpTitles

    result_count=$(wc -l "$cachedir"/tmpTitles | awk '{print $1}')
    if [ "$result_count" -lt 1 ]; then
      echo ":pensive: No Result found. Try again :red_circle:" -i "NONE"
      notify-send ":pensive: No Result found. Try again :red_circle:" -i "NONE"
      exit 0
    fi

    # Seeders and Leechers
    grep -o '<td class="coll-2 seeds.*</td>\|<td class="coll-3 leeches.*</td>' "$cachedir"/tmpHtml |
      sed 's/<[^>]*>//g' | sed 'N;s/\n/ /' > "$cachedir"/tmpSeed

    # Size
    grep -o '<td class="coll-4 size.*</td>' "$cachedir"/tmpHtml |
      sed 's/<span class="seeds">.*<\/span>//g' |
      sed -e 's/<[^>]*>//g' > "$cachedir"/tmpSize

    # Links
    grep -E '/torrent/' "$cachedir"/tmpHtml |
      sed -E 's#.*(/torrent/.*)/">.*/#\1#' |
      sed 's/td>//g' > "$cachedir"/tmpLink

    # Clearning up some data to display
    sed 's/\./ /g; s/\-/ /g' "$cachedir"/tmpTitles |
      sed 's/[^A-Za-z0-9 ]//g' | tr -s " " > "$cachedir"/tmp && mv "$cachedir"/tmp "$cachedir"/tmpTitles

    awk '{print NR " - ["$0"]"}' "$cachedir"/tmpSize > "$cachedir"/tmp && mv "$cachedir"/tmp "$cachedir"/tmpSize
    awk '{print "[S:"$1 ", L:"$2"]" }' "$cachedir"/tmpSeed > "$cachedir"/tmp && mv "$cachedir"/tmp "$cachedir"/tmpSeed

    # Getting the line number
    paste -d\   "$cachedir"/tmpSize "$cachedir"/tmpSeed "$cachedir"/tmpTitles > "$cachedir"/tmpMenu
    menu
    LINE=$(echo $query | cut -d\- -f1 | awk '{$1=$1; print}')

    if [ -z "$LINE" ]; then
      echo ":pensive: No Result selected. Exiting... :red_circle:" -i "NONE"
      notify-send ":pensive: No Result selected. Exiting... :red_circle:" -i "NONE"
      exit 0
    fi
    echo ":mag: Searching Magnet seeds :magnet:" -i "NONE"
    notify-send ":mag: Searching Magnet seeds :magnet:" -i "NONE"
    url=$(head -n $LINE "$cachedir"/tmpLink | tail -n +$LINE)
    fullURL="${baseurl}${url}/"

    # Requesting page for magnet link
    curl -s $fullURL > "$cachedir"/tmpHtml
    magnet=$(grep -Po "magnet:\?xt=urn:btih:[a-zA-Z0-9]*" "$cachedir"/tmpHtml | head -n 1)

    echo $magnet > "$cachedir"/last
    peerflix -k $magnet
}

# scrap index web pages for media
function indexwp {
    touch "$cachedir"/tmpHtml
    touch "$cachedir"/tmp
    touch "$cachedir"/tmpResult

    urls=$(cat "$msailordir"/source | grep "http" && find "$msailordir"/sync/ | grep "source" | cat $(cat) | grep "http")
    for url in $urls
    do
        curl -s $url -o "$cachedir"/tmpHtml
        suburl=$(grep -oP '(?<=<a href="(?!([/])|(\?.=.;.=.)|(\.{2}/))).*?(?=">)' "$cachedir"/tmpHtml)
        for line in $suburl
        do
            case $line in
                *".mkv"* | *".mp3"* | *".mp4"* | *".webm"* | *".avi"* | *".flv"* )
                    echo $line" "$url$line >> "$cachedir"/tmp
            esac
        done
    done

    if [ "$query" = ":indexwp" ]
    then
        cat "$cachedir"/tmp > "$cachedir"/tmpResult
    else
        grep $query -i "$cachedir"/tmp > "$cachedir"/tmpResult
    fi

    cat "$cachedir"/tmpResult > "$cachedir"/tmpMenu
    menu
    methods
}

# execute the functionality writen in $query
# params are passed in $query var
function methods {
    case $query in
        *":help"*)
            help ;;

        *":update"*)
            sudo curl -sL "https://gitlab.com/mediasailor/msailor_sh/-/raw/main/msailor.sh" -o /usr/local/bin/msailor ;;

        *":sync"*)
            sync ;;

        *":push"*)
            push ;;

        *":config-edit"*)
            $editor "$msailordir"/config ;;

        *":quickmark-add"*)
            query="${query/:quickmark-add /}" && [ "$query" != ":quickmark-add" ] && echo "$query $(cat "$cachedir"/last)" >> "$msailordir"/quickmark ;;
        *":quickmark-del"*)
            query="${query/:quickmark-del /}" && grep -v "$query" "$msailordir"/quickmark > "$cachedir"/tmp && cat "$cachedir"/tmp > "$msailordir"/quickmark ;;
        *":quickmark-edit"*)
            query="${query/:quickmark-edit /}" && $editor "$msailordir"/quickmark ;;

        *":list-add"*)
            query="${query/:list-add /}" && [ "$query" != ":list-add" ] && echo "$query" > "$cachedir"/tmpQuery && mkdir -p "$msailordir"/list && ls -1 "$msailordir"/list > "$cachedir"/tmpMenu && menu && touch "$msailordir"/list/$query && echo "$(cat "$cachedir"/tmpQuery) $(cat "$cachedir"/last)" >> "$msailordir"/list/$query ;;
        *":list-del"*)
            query="${query/:list-del /}" && [ "$query" != ":list-del" ] && rm -rf "$msailordir"/list/"$query" ;;
        *":list-edit"*)
            query="${query/:list-edit /}" && $editor "$msailordir"/list/"$query" ;;

        *":ytListImport"*)
            query="${query/:ytListImport /}" && ytListImport ;;
        *":yt"*)
            query="${query/:yt /}" && [ "$query" != ":yt" ] && yt ;;

        *":sc"*)
            query="${query/:sc /}" && [ "$query" != ":sc" ] && sc ;;

        *":torrent"*)
            query="${query/:torrent /}" && [ "$query" != ":torrent" ] && torrent ;;
        *":indexwp"*)
            query="${query/:indexwp /}" && indexwp ;;

        *"[file] "*)
            query="${query/\[file\] /}" && (killall mpv 2> /dev/null || true) && mpv "$msailordir"/file/$query ;;

        *"[list] "*)
            query="${query/\[list\] /}" && (cat "$msailordir"/list/$query | awk -F' ' '{print $NF}') > "$cachedir"/tmp | (killall mpv 2> /dev/null || true) && mpv $novideo --playlist="$cachedir"/tmp ;;
        *"[list-"*)
            echo $query | awk -F' ' '{print $1}' | awk -F'list-' '{print $2}' | awk -F']' '{print $1}' | (cat "$msailordir"/sync/$(cat)/list/"$(echo $query | awk -F' ' '{$1="";print $0}' | cut -c2-)" | awk -F' ' '{print $NF}') > "$cachedir"/tmp | (killall mpv 2> /dev/null || true) && mpv --playlist="$cachedir"/tmp ;;

        *"http"* | *"https"*)
            echo ${query##* } > "$cachedir"/last && (killall mpv 2> /dev/null || true) && mpv ${query##* } $novideo ;;
        *"magnet"*)
            echo ${query##* } > "$cachedir"/last && peerflix -k ${query##* } ;;
    esac
}

# print main menu and write command in $query
# params must be placed in "$cachedir"/tmpMenu
# return value in $query var
function menu {
    rm "$cachedir"/tmp
    while :
    do
        clear
        tput cup $(tput lines) 0

        # fix a bug that do not filter if there is a space
        if [ -s "$cachedir"/tmp ]; then
            cat "$cachedir"/tmpMenu | grep $(cat "$cachedir"/tmp) 2> /dev/null
        else
            cat "$cachedir"/tmpMenu
        fi

        echo ""
        echo -n "Media Sailor: "$(cat "$cachedir"/tmp 2> /dev/null)
        read -s -N 1 insert

        if [ "$insert" = $'\x1b' ]; then # [ESC] Pressed
            : > "$cachedir"/tmp
        elif [ "$insert" == $'\x7f' ] ; then # [Return] Pressed
            truncate -s-1 "$cachedir"/tmp
        elif [ "$insert" == $'\x09' ] ; then # [Tab] Pressed
            echo $(cat "$cachedir"/tmpMenu | grep $(cat "$cachedir"/tmp) | head -1) > "$cachedir"/tmp
            truncate -s-1 "$cachedir"/tmp
        elif [ "$insert" == $'\x0a' ] ; then # [Enter] Pressed
            query=$(cat "$cachedir"/tmp)
            break
        elif [ "$insert" == "[" ]; then # [Arrow] Pressed
            read -rsn1 -t 0.1 tmp
            case "$tmp" in
                "A") printf "Up\n";;
                "B") printf "Down\n";;
                "C") printf "Right\n";;
                "D") printf "Left\n";;
            esac
            break
        else
            echo $(cat "$cachedir"/tmp)"$insert" > "$cachedir"/tmp
            truncate -s-1 "$cachedir"/tmp
        fi

    done

    rm "$cachedir"/tmpMenu
    rm "$cachedir"/tmp
    echo $query
}

# start of the program

config

# check if params were passed
if [ -z $1 ]; then
    while :
    do
        find "$msailordir"/sync/ | grep "quickmark" | cut -d'/' -f7 > "$cachedir"/tmpUser

        echo '' > "$cachedir"/tmpMenu

        # synced list
        for user in $(cat "$cachedir"/tmpUser); do ls -1 "$msailordir"/sync/$user/list | awk -F'.' '{print "[list-'$user'] "$0}'; done >> "$cachedir"/tmpMenu
        # synced quickmark
        for user in $(cat "$cachedir"/tmpUser); do find "$msailordir"/sync/$user | grep "quickmark" | cat $(cat) | awk -F'.' '{print "[quickmark-'$user'] "$0}'; done  >> "$cachedir"/tmpMenu
        # list
        ls -1 "$msailordir"/list/ | awk '{print "[list] " $0}'  >> "$cachedir"/tmpMenu
        # quickmark
        cat "$msailordir"/quickmark | awk '{print "[quickmark] " $0}'  >> "$cachedir"/tmpMenu
        # local files
        ls -1 "$msailordir"/file | awk '{print "[file] " $0}'  >> "$cachedir"/tmpMenu

        # commands
        echo '' >> "$cachedir"/tmpMenu
        echo ':help' >> "$cachedir"/tmpMenu
        echo ':update' >> "$cachedir"/tmpMenu
        echo ':sync' >> "$cachedir"/tmpMenu
        echo ':push' >> "$cachedir"/tmpMenu
        echo ':config-edit' >> "$cachedir"/tmpMenu
        echo ':quickmark-add' >> "$cachedir"/tmpMenu
        echo ':quickmark-del' >> "$cachedir"/tmpMenu
        echo ':quickmark-edit' >> "$cachedir"/tmpMenu
        echo ':list-add' >> "$cachedir"/tmpMenu
        echo ':list-del' >> "$cachedir"/tmpMenu
        echo ':list-edit' >> "$cachedir"/tmpMenu
        echo ':yt' >> "$cachedir"/tmpMenu
        echo ':ytListImport' >> "$cachedir"/tmpMenu
        echo ':sc' >> "$cachedir"/tmpMenu
        echo ':torrent' >> "$cachedir"/tmpMenu
        echo ':indexwp' >> "$cachedir"/tmpMenu

        menu
        methods

        rm "$cachedir"/tmp*
    done
else
    query=$1
    [[ $query == "--help" || $query == "-h" ]] && help
    methods

    rm "$cachedir"/tmp* 2> /dev/null
fi

