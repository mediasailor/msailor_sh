# msailor

- [info](#info)
- [requirements](#requirements)
- [install](#installation)
- [usage](#usage)

## Info

This is a shell script. It searchs in youtube, soundcloud, torrents, index web pages, locally... and open the media in mpv.
For scraping, the script uses simple gnu utils like sed, awk, paste, cut.

## Requirements

* [git](https://git-scm.com/)
* [mpv](https://mpv.io/) - A free, open source, and cross-platform media player.

For youtube and soundcloud:
* [yt-dlp](https://github.com/yt-dlp/yt-dlp) - Command-line program to download videos from YouTube.com and other video sites.

For torrents:
* [peerflix](https://github.com/mafintosh/peerflix) - Streaming torrent client for node.js.

## Installation

### cURL
- cURL **msailor** to your **$PATH** and give execute permissions.
- set pseudo-gui in mpv config so it will open anyway with audio files.

```sh
sudo curl -sL "https://gitlab.com/mediasailor/msailor_sh/-/raw/main/msailor.sh" -o /usr/local/bin/msailor
sudo chmod +x /usr/local/bin/msailor
```

- To update, just do `curl` again, no need to `chmod` anymore.
- To uninstall, simply remove `msailor` from your **$PATH**, for example `sudo rm -f /usr/local/bin/msailor

## Usage

msailor got --help and :help function to display how it works.
All configuration, downloaded files and synced repos are inside $XDG_CONFIG_HOME/msailor or ~/.config/msailor if $XDG_CONFIG_HOME is not set.
- msailor folder example:
```
.../msailor
   | config (your config file)
   | source (file containing index web pages to scrap from)
   | quickmark (your quickmarks)
   | list/ (folder containing your media lists)
   | sync/ (folder that stores your git synced repos)
```

## sync msailor folder to git repo

You can sync your msailor folder to a repo so others can sync it and you got it saved in a git repo. Use the next .gitignore to keep it clean.

.gitignore:
```
/*
!.gitignore
!config
!quickmark
!source
!list/
```

## config

config is the config file of the app. You can sync this file in your msailor repo.

Options available in config:
```
set novideo
set editor=[args]

sync=https://host.com/path/to/the/git.git
sync=https://host.com/path/to/the/git.git
sync=https://host.com/path/to/the/git.git
...
```
### sync

Insert in your config the links of the git repositories you want to sync.
e.g.
```
sync=https://host.com/path/to/the/git.git
sync=https://host.com/path/to/the/git.git
sync=https://host.com/path/to/the/git.git
sync=https://host.com/path/to/the/git.git
sync=https://host.com/path/to/the/git.git
```
Use sync command to remove and download again all the repos. They will be stored inside .msailor/sync/

## Credits
- I took this idea after saw [Bugswritter's notflix](https://github.com/Bugswriter/notflix), take a look at his code !.
- [BrutalSphere](https://gitlab.com/BrutalSphere) contributed to this project.
